kind: script

build-depends:
- vm/deploy-tools.bst
- vm/boot/efi.bst
- vm/minimal/filesystem.bst
- vm/minimal/initial-scripts.bst
- vm/prepare-image.bst

environment:
  E2FSPROGS_FAKE_TIME: '%{source-date-epoch}'

variables:
  uuidnamespace: df2427db-01ec-4c99-96b1-be3edb3cd9f6
  (?):
    - target_arch == "x86_64":
        linux-root: 4F68BCE3-E8CD-4DB1-96E7-FBCAF984B709
    - target_arch == "i686":
        linux-root: 44479540-F297-41B2-9AF7-D131D5F0458A
    - target_arch == "arm":
        linux-root: 69DAD710-2CE4-4E3C-B16C-21A1D49ABED3
    - target_arch == "aarch64":
        linux-root: B921B045-1DF0-41C3-AF44-4C6F280D3FAE
    - target_arch == "riscv64":
        linux-root: 72EC70A6-CF74-40E6-BD49-4BDA08E8F224

config:
  layout:
  - element: ''
    destination: '/genimage'
  - element: ''
    destination: '/tmp'
  - element: 'vm/deploy-tools.bst'
    destination: '/'
  - element: 'vm/boot/efi.bst'
    destination: '/sysroot/efi'
  - element: 'vm/minimal/filesystem.bst'
    destination: '/sysroot'
  - element: 'vm/minimal/initial-scripts.bst'
    destination: '/'

  commands:
  - |
    cp -a /sysroot /genimage/root

  - |
    prepare-image.sh \
       --sysroot /genimage/root \
       --seed "%{uuidnamespace}" \
       --rootpasswd "root" >/tmp/vars

  - |
    find /genimage/root -depth -exec touch -h --date="@${SOURCE_DATE_EPOCH}" {} ";"

  - |
    efi_part_uuid="$(uuidgen -s --namespace "%{uuidnamespace}" --name partition-efi)"
    root_part_uuid="$(uuidgen -s --namespace "%{uuidnamespace}" --name partition-root)"
    disk_uuid="$(uuidgen -s --namespace "%{uuidnamespace}" --name disk)"
    hash_seed="$(uuidgen -s --namespace "%{uuidnamespace}" --name hash-seed)"
    . /tmp/vars
    cat >/genimage/genimage.cfg <<EOF
    image efi.img {
        vfat {
            extraargs = "--invariant -F32 -i${id_efi} -n EFI"
        }
        mountpoint = "/efi"
        size = 100M
    }
    image root.img {
        ext4  {
            label = "root"
            extraargs = "-U ${uuid_root} -E hash_seed=${hash_seed}"
            use-mke2fs = true
            fs-timestamp = "${SOURCE_DATE_EPOCH}"
        }
        size = 1G
    }
    image disk.img {
        hdimage {
            align = 1M
            gpt = true
            disk-uuid = "${disk_uuid}"
        }
        partition efi {
            image = "efi.img"
            partition-type-uuid = "U"
            partition-uuid = "${efi_part_uuid}"
        }
        partition root {
            image = "root.img"
            partition-type-uuid = "%{linux-root}"
            partition-uuid = "${root_part_uuid}"
        }
    }
    EOF

  - |
    cd /genimage/
    genimage

  - |
    install -Dm644 -t "%{install-root}" /genimage/images/disk.img
